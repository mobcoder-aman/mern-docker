const express = require('express');

const connectDB = require('./config/db')

const dotenv = require('dotenv')

var cors = require('cors')

dotenv.config();

const app = express();

// Connect Database
connectDB();

// Init Middleware
app.use(express.json({extended:true}));

app.use(cors());

app.get('/' , (req ,res) => res.send('API RUNNING123'));

// Define Routes
app.use('/api/v1/users' , require('./routes/user'));
app.use('/api/v1/product' , require('./routes/product'));

app.use('/' , require('./routes/facebook_login'));

// app.use('/api/profile' , require('./routes/profile'));
// app.use('/api/v1/auth' , require('./routes/auth'));
// app.use('/api/posts' , require('./routes/posts'));


const PORT = process.env.SERVER_PORT || 3001 ;

app.listen(PORT , () => console.log(`SERVER STARTED ON PORT ${PORT}`));


 